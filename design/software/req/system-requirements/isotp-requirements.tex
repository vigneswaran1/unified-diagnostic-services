
\section{ISO-TP Protocol Specification}

	\gls{iso} 15765-2 or \gls{isotp} defines sending data packets over a \gls{can} bus.

	This protocol allows for the transport of messages that exceed the eight-byte maximum payload of \gls{can} frames.

  \gls{isotp} segments longer messages into multiple frames, adding metadata that allows the interpretation of individual frames and reassembly into a complete message packet by the recipient.
  
  It can carry up to 4095 bytes of payload per message packet.

\begin{figure}[H]
  \centering
  \includegraphics[width=3.0in]{figures/udsprotocollayers}
  \caption{\gls{uds} Protocol Layers}
  \label{UDS Protocol Layers}
\end{figure}

\subsection {Main Functionalities}
  \begin{enumerate}
    \item \requirement{REQ-MAIN-ISOTP.2001} Transmission of messages up to 4095 bytes. (refer -constraint)
    \item \requirement{REQ-MAIN-ISOTP.2002} Reception of messages up to 4095 bytes. (refer -constraint)
    \item \requirement{REQ-MAIN-ISOTP.2003} Reports transmission complete to upper layer.
    \item \requirement{REQ-MAIN-ISOTP.2004} Reports transmission failure to upper layer.
    \item \requirement{REQ-MAIN-ISOTP.2005} Reports reception complete to upper layer.
    \item \requirement{REQ-MAIN-ISOTP.2006} Reports reception failure to upper layer.
  \end{enumerate}
\subsection {Features}

  \begin{enumerate}
  \item \requirement{REQ-ISOTP.2001} The protocol shall provide the following features.
  \begin{enumerate}
    \item Platform agnostic
    \item Low foot print
    \item \gls{misra} C compliant
    \item Non-blocking implementation
    \item Configurable \gls{isotp} stack services
    \item Error handling according to \gls{iso} 15765
  \end{enumerate}
\end{enumerate}

\begin{figure}[H]
  \centering
  \includegraphics[width=3.0in]{figures/udsprotocollayers}
  \caption{Service Manager}
  \label{Service Manager}
\end{figure}

\subsection {ISO 15765 Overview}

  \gls{iso} 15765 is physical layer, datalink layer, and transport protocol for OBD-2, EOBD, \gls{uds}, and KWP2000. It is used in all passenger cars and is used world-wide. 
  As of 2008, \gls{iso} 15765 replaces both versions of SAE J1850 and \gls{iso} 9141.
  \gls{iso} 15765 is a low-level communications protocol, which operates on a \gls{can}. 
  \gls{iso} 15765 specifies exactly how messages are exchanged between \gls{ecu} on a vehicle.
  It defines a messages priority, source address, destination address, size, and how large messages are fragmented and reassembled. For example, \gls{iso} 15765-4 specifies the engine to be address 0.

  ISO 15765 is made up of the following specifications:
  \begin{enumerate}
    \item ISO 15765-2: Network layer services
    \item ISO 15765-3: \gls{uds}
    \item ISO 15765-4: Requirements for emissions-related systems
  \end{enumerate}

\subsection {ISO 15765 Frames}

  At the heart of \gls{iso} 15765 is the \gls{pdu}, which is a number assigned to every type of frame. The \gls{pdu} serves to identify single frames, first frames, consecutive frames, and flow control frames. A message will be consisting of single or multiple frames, and it is the \gls{pdu} which encodes that information.
\paragraph {\gls{iso} 15765 SF \gls{pdu}}
  \gls{sf} is used when a message is sent out as a single frame. This is the simplest form of encoding used in \gls{iso} 15765. Message lengths of 1 to 7 bytes will use the single frame \gls{pdu}.

  \begin{figure}[H]
    \centering
    \includegraphics[width=3.0in]{figures/sfframe}
    \caption{ISOTP-Start Frame PDU}
    \label{Start Frame PDU}
  \end{figure}

\paragraph {\gls{iso} 15765 FF \gls{pdu}}
  \gls{ff} \gls{pdu} is used when a message is sent out as a multiple frame. This is the first frame sent in the exchange between nodes. Message lengths of 8 to 4095 bytes will use the \gls{ff}, \gls{cf}, and \gls{fc} \gls{pdu}.

  \begin{figure}[H]
  \centering
  \includegraphics[width=3.0in]{figures/ffframe}
  \caption{ISOTP-First Frame PDU}
  \label{First Frame PDU}
  \end{figure}

\paragraph {\gls{iso} 15765 \gls{cf} \gls{pdu}}
  \gls{cf} \gls{pdu} is used when a message is sent out as a multiple frame. This is the frame sent repeated in the exchange between nodes and contains the actual data for the complete message.

  \begin{figure}[H]
  \centering
  \includegraphics[width=3.0in]{figures/cfframe}
  \caption{ISOTP-Consecutive Frame PDU}
  \label{Consecutive Frame PDU}
  \end{figure}

\paragraph {\gls{iso} 15765 \gls{fc} \gls{pdu}} \mbox{} \\
  \gls{fc} \gls{pdu} is used to adjust the sender to the network layer capabilities of the receiver. This flow control scheme allows the use of diagnostic gateways and sub-networks.

  \begin{figure}[H]
  \centering
  \includegraphics[width=3.0in]{figures/fcframe}
  \caption{ISOTP-Flow Control Frame PDU}
  \label{Flow Control Frame PDU}
  \end{figure}

\subsection {Software Design}
CAN TP provides services for
  \begin{enumerate}
      \item \requirement{REQ-SW-DESIGN-ISOTP.2001} Segmentation of data in transmit direction
      \item \requirement{REQ-SW-DESIGN-ISOTP.2002} Reassembling of data in receive direction
      \item \requirement{REQ-SW-DESIGN-ISOTP.2003} Control of data flow
      \item \requirement{REQ-SW-DESIGN-ISOTP.2004} Detection of errors in segmentation sessions
      \item \requirement{REQ-SW-DESIGN-ISOTP.2005} Transmit cancellation
      \item \requirement{REQ-SW-DESIGN-ISOTP.2006} Receive cancellation
  \end{enumerate}

\subsubsection {ISOTP Architecture}
\begin{figure}[H]
  \centering
  \includegraphics[width=3.0in]{figures/isotparchitecture}
  \caption{ISOTP Architecture}
  \label{ISOTP Architecture}
\end{figure}

\subsubsection {Receive Segmented Message}
 \begin{figure}[H]
  \centering
  \includegraphics[width=5.0in]{figures/isotp-framing}
  \caption{ISO TP- Segmented Frame receive}
  \label{ISO TP - Segmented Frame receive}
\end{figure}

\subsubsection {Software Components}
  Following are the software components present
  \begin{enumerate}
    \item \gls{can} driver
    \item \gls{can} driver wrapper
    \item \gls{isotp} stack
    \item \gls{uds} Glue layer
    \item \gls{isotp} Test module
  \end{enumerate}

\subsubsection {CAN driver}
The \gls{can} driver shall be used from the existing code. When porting to new processor the driver shall be  written new.

\subsubsection {CAN driver wrapper}
This is the \gls{hal} for the \gls{can} driver wrapper which links the \gls{isotp} and \gls{can} driver. The wrapper layer is defined to isolated and loosely couple \gls{can} driver from the other layer. 
Following are the functions in wrapper layer.

\paragraph {Receive_Isotp}
  Wrapper for CAN RX. According to the platform and CAN driver this function needs to be modified by the user. 
\paragraph {Send_Isotp}
  Wrapper for CAN TX. According to the platform and CAN driver this function needs to be modified by the user.
\paragraph {Receive_test_frame}
  Test routine for ISO_TP test module interface. This will directly populate the test packet to the receive data structure. This will be used only in test module.
\paragraph {Send_test_frame}
  This test routine is for ISO_TP test module interface. This will be used only in test module to read the test packet from the ISO_TP stack.
\paragraph {Read_data_buffer}
  This test routine will read the static receive buffer of the ISO-TP stack. This will be used only in test module to verify the multi packet data assembly.
  CAN Driver wrapper is the only layer that will be linked with CAN Driver layer. The ISO-TP stack will interface with CAN Driver wrapper. When ISO-TP is ported to different platform, this layer will be the only one that requires modification.

\subsubsection {ISO-TP stack}
\subsubsection {UDS Glue layer}
\subsubsection {ISO-TP Test module}

\subsubsection {Error Handling}
  \begin{enumerate}
    \item Invalid frame type - 0x00 to 0x03 is valid
    \item Incorrect frame length. CAN Data length should be validated against the data length of each packet
    \item Incorrect sequence number in multi packet frame
    \item Incorrect flow status values in flow control
  \end{enumerate}

\subsubsection {Application Interface}
\paragraph {Communication services} \mbox{} \\
  \begin{enumerate}
    \item \requirement{REQ-SW-DESIGN-ISOTP.2101} \gls{isotp} shall implement a service to request transfer of data (N_USData.request) for \gls{uds} or application.
    \item \requirement{REQ-SW-DESIGN-ISOTP.2102} \gls{isotp} shall implement a service to receive the first frame signal of a segmented message reception (N_USData_FF.indication) for \gls{uds} or application.
    \item \requirement{REQ-SW-DESIGN-ISOTP.2103} \gls{isotp} shall implement a service (N_USData.indication) to receive data for \gls{uds} or application.
    \item \requirement{REQ-SW-DESIGN-ISOTP.2104} \gls{isotp} shall implement a service (N_USData.confirm), which confirm the previous request services has been executed (success/fail) for \gls{uds} or application to request transfer of data.

  \end{enumerate}

  \begin{figure}[H]
  \centering
  \includegraphics[width=3.0in]{figures/uds-isotp-interface}
  \caption{ISO TP- Application services}
  \label{ISO TP - Application services}
  \end{figure}