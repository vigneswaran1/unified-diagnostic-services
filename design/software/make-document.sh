#!/bin/bash
input_file=$1
glossary_file=${input_file%.tex}
rm -rf ./build/
mkdir build/
xelatex -aux-directory=./build/ -output-directory=./build/ $input_file
makeglossaries.exe -d ./build/ $glossary_file
xelatex -aux-directory=./build/ -output-directory=./build/ $input_file
